﻿export interface Discussion {
    Observer: string; // required with minimum 5 chracters
    Date: Date;
    Location: string;
    Collegue: string;
    Subject: string;
    Outcomes: string;
   
}
 