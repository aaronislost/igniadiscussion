﻿import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule, provideRoutes } from '@angular/router';

 

import { discussionAddEditComponent } from './addEdit/discussionAddEdit.component';
import { DiscussionService } from './discussion.service';

@NgModule({
    imports: [CommonModule],
    declarations: [
        discussionAddEditComponent,
        DiscussionService
    ],
    providers: [DiscussionService],
    exports: [discussionAddEditComponent]
})
export class CreditCardModule { }