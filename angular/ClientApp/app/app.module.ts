
import { NgModule } from '@angular/core';
import { RouterModule, provideRoutes } from '@angular/router';
import { UniversalModule } from 'angular2-universal';
import { AppComponent } from './components/app/app.component'
import { NavMenuComponent } from './components/navmenu/navmenu.component';
import { HomeComponent } from './components/home/home.component';
import { FetchDataComponent } from './components/fetchdata/fetchdata.component';
import { CounterComponent } from './components/counter/counter.component';
import { discussionAddEditComponent } from './components/discussions/addEdit/discussionAddEdit.component';
//import { DiscussionRoutingModule, discussionRoutedComponents } from './components/discussions/discussion.routing';
import { discussionListComponent } from './components/discussions/list/discussionList.component';


@NgModule({
    bootstrap: [ AppComponent ],
    declarations: [
        AppComponent,
        NavMenuComponent,
        CounterComponent,
        FetchDataComponent,
        HomeComponent,
        discussionAddEditComponent,
        discussionListComponent
 //       DiscussionRoutingModule,
 //       discussionRoutedComponents
    ],
    imports: [
        UniversalModule, // Must be first import. This automatically imports BrowserModule, HttpModule, and JsonpModule too.
        
        RouterModule.forRoot([
            { path: '', redirectTo: 'home', pathMatch: 'full' },
            { path: 'home', component: HomeComponent },
            { path: 'counter', component: CounterComponent },
            { path: 'discussions', component: discussionListComponent },
            { path: 'discussion/:id', component: discussionAddEditComponent }
            { path: 'fetch-data', component: FetchDataComponent },
            //{ path: '**', redirectTo: 'home' }
        ])
    ]
})
export class AppModule {
}
