import { Component } from '@angular/core';
import { Discussion } from '../../Discussion';
import { DiscussionService } from '../discussion.service';
import { FormBuilder, FormGroup, FormControl, Validators } from '@angular/forms';

@Component({
    selector: ' ',
    templateUrl: './discussionAddEdit.component.html'
})

export class discussionAddEditComponent {

    form: FormGroup;

    constructor(
        public fb: FormBuilder,
        private discussionService: DiscussionService
    ) { }

    ngOnInit() {
        alert('hello');

        this.form = this.fb.group({
            name: ['', Validators.required],
            event: this.fb.group({
                title: ['', Validators.required],
                location: ['', Validators.required]
            })
        });
 
    }

    public save(model: Discussion) {
        alert('here');
    }
}
